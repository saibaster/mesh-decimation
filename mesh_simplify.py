import sys
import os
import subprocess

# Find script by going to Filter > Show current filter script (in MeshLab)

# <filter name="Simplification: Quadric Edge Collapse Decimation (with texture)">
filter_script_mlx = """<!DOCTYPE FilterScript>
<FilterScript>
<filter name="Remove Unreferenced Vertex"/>
<filter name="Quadric Edge Collapse Decimation">
  <Param type="RichInt" value="2000" name="TargetFaceNum"/>
  <Param type="RichFloat" value="0.0" name="TargetPerc"/>
  <Param type="RichFloat" value="0.3" name="QualityThr"/>
  <Param type="RichBool" value="true" name="PreserveBoundary"/>
  <Param type="RichFloat" value="1" name="BoundaryWeight"/>
  <Param type="RichBool" value="true" name="PreserveNormal"/>
  <Param type="RichBool" value="true" name="PreserveTopology"/>
  <Param type="RichBool" value="true" name="OptimalPlacement"/>
  <Param type="RichBool" value="true" name="PlanarQuadric"/>
  <Param type="RichBool" value="false" name="QualityWeight"/>
  <Param type="RichBool" value="true" name="AutoClean"/>
  <Param type="RichBool" value="false" name="Selected"/>
 </filter>
 <filter name="Laplacian Smooth">
  <Param type="RichInt" value="3" name="stepSmoothNum"/>
  <Param type="RichBool" value="true" name="Boundary"/>
  <Param type="RichBool" value="true" name="cotangentWeight"/>
  <Param type="RichBool" value="false" name="Selected"/>
 </filter>
<filter name="Remove Unreferenced Vertex"/>
<filter name="Remove Duplicated Vertex"/>
<filter name="Remove Duplicate Faces"/>
</FilterScript>
"""

cwd = os.getcwd()

def create_tmp_filter_file(filename='filter_file_tmp.mlx'):
    with open(cwd + '/' + filename, 'w') as f:
        f.write(filter_script_mlx)
    return cwd + '/' + filename


def reduce_faces(in_file,
                 out_file,
                 filter_script_path=create_tmp_filter_file()):
    # print("Out file:")
    # print(out_file)
    # Add input mesh
    command = "meshlabserver -i \"" + in_file + "\""
    # Add the filter script
    command += " -s " + "\"" + filter_script_path + "\""
    # Add the output filename and output flags
    command += " -o " + "\"" + out_file + "\""
    # Execute command
    # print("\nGoing to execute: " + command)
    output = subprocess.call(command, shell=True)
    last_line = output
    # print("\nDone:\n")
    # print(in_file + " > " + out_file + ": " + str(last_line))


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage:")
        print(sys.argv[0] + " /path/to/input_mesh")
        print("For example:")
        print(sys.argv[0] + " /home/myuser/mymesh.obj")
        exit(0)

    in_mesh = sys.argv[1]
    filename = in_mesh.split('/')[-1]
    # output = filename[:-4] + "_decimated.obj"

    folder_name = cwd
    #out_mesh = folder_name + "/mesh_decimated.obj"
    out_mesh = "mesh_decimated.obj"

    reduce_faces(in_mesh, out_mesh)